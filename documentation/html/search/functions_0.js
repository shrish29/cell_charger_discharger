var searchData=
[
  ['calculate_5favg',['calculate_avg',['../charger__discharger_8c.html#a6f1efdf53830030b8d4b67843aad1870',1,'calculate_avg():&#160;charger_discharger.c'],['../charger__discharger_8h.html#ab2f580a1d85442504950b07e3c8f1145',1,'calculate_avg(void):&#160;charger_discharger.c']]],
  ['cc_5fcv_5fmode',['cc_cv_mode',['../charger__discharger_8c.html#a9ab308304942d68ad5733f1a0a9685a4',1,'cc_cv_mode(float current_voltage, unsigned int reference_voltage, char CC_mode_status):&#160;charger_discharger.c'],['../charger__discharger_8h.html#a9ab308304942d68ad5733f1a0a9685a4',1,'cc_cv_mode(float current_voltage, unsigned int reference_voltage, char CC_mode_status):&#160;charger_discharger.c']]],
  ['cell_5foff',['Cell_OFF',['../charger__discharger_8c.html#a3290085338ff7657c1ff25788d944727',1,'Cell_OFF():&#160;charger_discharger.c'],['../charger__discharger_8h.html#a5bbc85db8cf08142e0984d07b97e722b',1,'Cell_OFF(void):&#160;charger_discharger.c']]],
  ['cell_5fon',['Cell_ON',['../charger__discharger_8c.html#a1d26bd6e1261ba0d776c492ad35593f4',1,'Cell_ON():&#160;charger_discharger.c'],['../charger__discharger_8h.html#a75740b5600396316399a5608fe2cb4d3',1,'Cell_ON(void):&#160;charger_discharger.c']]],
  ['control_5floop',['control_loop',['../charger__discharger_8c.html#a0c80a804f63cdf1b40bca055d0b2d77d',1,'control_loop():&#160;charger_discharger.c'],['../charger__discharger_8h.html#a4f05187afaedd0cd35e9137eaf557ef8',1,'control_loop(void):&#160;charger_discharger.c']]],
  ['converter_5fsettings',['converter_settings',['../charger__discharger_8h.html#a116216d02b45c1207161f503bf20adbc',1,'converter_settings(void):&#160;state_machine.c'],['../state__machine_8c.html#a381231cb2e4b3c61e21b04f646cfab92',1,'converter_settings():&#160;state_machine.c']]]
];
