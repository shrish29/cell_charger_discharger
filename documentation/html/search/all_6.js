var searchData=
[
  ['i',['i',['../charger__discharger_8h.html#a58e739b7bdf8275493686ae76e3705c3',1,'charger_discharger.h']]],
  ['i_5f1_5fdcres',['i_1_dcres',['../charger__discharger_8h.html#a51e77c4807be0e46b1594a0263acaf36',1,'charger_discharger.h']]],
  ['i_5f2_5fdcres',['i_2_dcres',['../charger__discharger_8h.html#a8e61d5ae14f01d57c4b14204727ed8f8',1,'charger_discharger.h']]],
  ['i_5fchan',['I_CHAN',['../charger__discharger_8h.html#a72da0b033e023b574cca6ab663e25fca',1,'charger_discharger.h']]],
  ['i_5fchar',['i_char',['../charger__discharger_8h.html#a9f7bc04927df757f9416084659beb22b',1,'charger_discharger.h']]],
  ['i_5fdisc',['i_disc',['../charger__discharger_8h.html#abee5b899e99e0165d63f1e4a3b72a9dd',1,'charger_discharger.h']]],
  ['idle',['IDLE',['../charger__discharger_8h.html#aa19be6305a5a4485e1e70de70ed7d677afd6a0e4343048b10646dd2976cc5ad18',1,'charger_discharger.h']]],
  ['initialize',['initialize',['../charger__discharger_8c.html#a25a40b6614565f755233080a384c35f1',1,'initialize():&#160;charger_discharger.c'],['../charger__discharger_8h.html#a9efe22aaead3a5e936b5df459de02eba',1,'initialize(void):&#160;charger_discharger.c']]],
  ['integral',['integral',['../charger__discharger_8h.html#a1174af01b8a329a6d465ea6e0951b3e5',1,'charger_discharger.h']]],
  ['ip_5fbuff',['ip_buff',['../charger__discharger_8h.html#a639c6c3902af09dac255279f04f0db89',1,'charger_discharger.h']]],
  ['iprom',['iprom',['../charger__discharger_8h.html#afce1f09585094a8102cf627d00de6d6f',1,'charger_discharger.h']]],
  ['iref',['iref',['../charger__discharger_8h.html#ab6bab6490e9e38ab60181b5cda64c53b',1,'charger_discharger.h']]],
  ['isdone',['ISDONE',['../charger__discharger_8h.html#aa19be6305a5a4485e1e70de70ed7d677a2df4f4d04ec310a30dd8eaac64f883a2',1,'charger_discharger.h']]]
];
